<?php

namespace AppBundle\Services\XmlReader;

use SimpleXMLElement;

/**
 * Class XmlReader
 * @package AppBundle\Services\XmlReader
 */
class XmlReader implements XmlReaderInterface
{

    /**
     * @var string
     */
    private $file;

    /**
     * @var bool|string
     */
    private $xml;

    /**
     * @var SimpleXMLElement
     */
    private $simpleXmlElement;


    //$file = $this->get('kernel')->getRootDir() . '/Resources/data/' . 'r2response.xml'


    /**
     * XmlReader constructor.
     *
     * @param string $file
     */public function __construct(string $file)
    {
        $this->file = $file;
        $this->xml = file_get_contents($file);
        $this->simpleXmlElement = simplexml_load_string($this->xml);
    }


    /**
     * @return SimpleXMLElement
     * @throws \Exception
     */
    public function getSimpleXmlElement(): SimpleXMLElement
    {
        if (!$this->simpleXmlElement) {
            throw new \Exception('SimpleXML Element does not exist');
        }

        return $this->simpleXmlElement;
    }

}