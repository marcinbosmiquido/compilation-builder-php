<?php

namespace AppBundle\Services\XmlReader;

use SimpleXMLElement;

interface XmlReaderInterface
{

    /**
     * @throws \Exception
     * @return SimpleXMLElement
     */
    public function getSimpleXmlElement(): SimpleXMLElement;
}