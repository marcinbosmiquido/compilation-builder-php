<?php

namespace AppBundle\Services\Database\ArtistRole;

use AppBundle\Services\Transformer\Resource\ArtistRolesResourceInterface;

/**
 * Interface ArtistRoleRepositoryInterface
 * @package AppBundle\Services\Database\ArtistRole
 */
interface ArtistRoleRepositoryInterface
{

    /**
     * @param array $roles
     *
     * @return void
     */
    public function addRoles(array $roles): void;
    /**
     * @return ArtistRolesResourceInterface
     */
    public function getArtistRoles(): ArtistRolesResourceInterface;
    /**
     * @throws \Exception
     */
    public function checkIfNotEmpty(): void;
}