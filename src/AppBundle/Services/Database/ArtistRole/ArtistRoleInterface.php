<?php

namespace AppBundle\Services\Database\ArtistRole;

/**
 * Interface ArtistRoleInterface
 * @package AppBundle\Services\Database\ArtistRole
 */
interface ArtistRoleInterface
{

    /**
     * @return int
     */
    public function getId(): int;


    /**
     * @return string
     */
    public function getName(): string;


    /**
     * @return string
     */
    public function getSlug(): string;
}