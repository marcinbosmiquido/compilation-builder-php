<?php

namespace AppBundle\Services\Database\Genre;

interface GenreInterface
{

    /**
     * Get id
     *
     * @return int
     */
    public function getId(): int;


    /**
     * Get name
     *
     * @return string
     */
    public function getName(): string;


    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug(): string;
}