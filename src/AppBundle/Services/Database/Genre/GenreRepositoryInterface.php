<?php

namespace AppBundle\Services\Database\Genre;

use AppBundle\Services\Transformer\Resource\GenresResourceInterface;

/**
 * Interface GenreRepositoryInterface
 * @package AppBundle\Services\Database\Genre
 */
interface GenreRepositoryInterface
{

    /**
     * @param array $genres
     *
     * @return void
     */
    public function addGenres(array $genres): void;


    /**
     * @return GenresResourceInterface
     */
    public function getGenres(): GenresResourceInterface;


    /**
     * @throws \Exception
     */
    public function checkIfNotEmpty(): void;
}

