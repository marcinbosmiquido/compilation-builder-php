<?php declare(strict_types=1);

namespace AppBundle\Services\Transformer\Api;

use AppBundle\Services\Database\ArtistRole\ArtistRoleInterface;
use AppBundle\Services\Transformer\Resource\ArtistRolesResourceInterface;

/**
 * Class ArtistRolesTransformer
 * @package AppBundle\Services\Transformer\Api
 */
class ArtistRolesTransformer implements ArtistRolesTransformerInterface
{

    /**
     * @param ArtistRolesResourceInterface $artistRolesResource
     *
     * @return array
     * @throws \Exception
     */
    public function transform(ArtistRolesResourceInterface $artistRolesResource): array
    {

        return array_map(function (ArtistRoleInterface $artistRole) {
           return [
               'id'     => $artistRole->getId(),
               'name'   => $artistRole->getName(),
               'slug'   => $artistRole->getSlug(),
           ];
        }, $artistRolesResource->getRoles());
    }

}