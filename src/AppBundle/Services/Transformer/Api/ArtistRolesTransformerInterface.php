<?php

namespace AppBundle\Services\Transformer\Api;

use AppBundle\Services\Transformer\Resource\ArtistRolesResourceInterface;

/**
 * Interface ArtistRolesTransformerInterface
 * @package AppBundle\Services\Transformer\Api
 */
interface ArtistRolesTransformerInterface
{

    /**
     * @param ArtistRolesResourceInterface $artistRolesResource
     *
     * @return array
     */
    public function transform(ArtistRolesResourceInterface $artistRolesResource): array;
}