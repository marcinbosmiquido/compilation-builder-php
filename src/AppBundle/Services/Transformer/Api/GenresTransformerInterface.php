<?php

namespace AppBundle\Services\Transformer\Api;

use AppBundle\Services\Transformer\Resource\GenresResourceInterface;

/**
 * Interface GenresTransformerInterface
 * @package AppBundle\Services\Transformer\Api
 */
interface GenresTransformerInterface
{

    /**
     * @param GenresResourceInterface $genresResource
     *
     * @return array
     */
    public function transform(GenresResourceInterface $genresResource): array;
}