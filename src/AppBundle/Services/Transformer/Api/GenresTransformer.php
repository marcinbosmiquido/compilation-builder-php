<?php

namespace AppBundle\Services\Transformer\Api;

use AppBundle\Services\Transformer\Resource\GenresResourceInterface;

/**
 * Class GenresTransformer
 * @package AppBundle\Services\Transformer\Api
 */
class GenresTransformer implements GenresTransformerInterface
{

    /**
     * @param GenresResourceInterface $genresResource
     *
     * @return array
     * @throws \Exception
     */
    public function transform(GenresResourceInterface $genresResource): array
    {
        $transformedGenres = [];

        foreach ($genresResource->getGenres() as $genre) {
            $transformedGenres[] = [
                'id'    => $genre->getId(),
                'name'  => $genre->getName(),
                'slug'  => $genre->getSlug(),
            ];
        }

        return $transformedGenres;
    }

}