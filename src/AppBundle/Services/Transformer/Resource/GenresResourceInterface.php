<?php

namespace AppBundle\Services\Transformer\Resource;

use AppBundle\Services\Database\Genre\GenreInterface;

/**
 * Interface GenresResourceInterface
 * @package AppBundle\Services\Transformer\Resource
 */
interface GenresResourceInterface
{

    /**
     * @return GenreInterface[]
     * @throws \Exception
     */
    public function getGenres(): array;
}