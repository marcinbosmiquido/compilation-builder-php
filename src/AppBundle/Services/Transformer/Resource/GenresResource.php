<?php

namespace AppBundle\Services\Transformer\Resource;

use AppBundle\Services\Database\Genre\GenreInterface;

/**
 * Class GenresResource
 * @package AppBundle\Services\Transformer\Resource
 */
class GenresResource implements GenresResourceInterface
{

    /**
     * @var GenreInterface[]
     */
    private $genres = [];


    /**
     * GenresResource constructor.
     *
     * @param array $genres
     */
    public function __construct(array $genres)
    {
        foreach ($genres as $genre) {
            $this->addGenre($genre);
        }
    }


    /**
     * @param GenreInterface $genre
     */
    private function addGenre(GenreInterface $genre): void
    {
        $this->genres[] = $genre;
    }


    /**
     * @return GenreInterface[]
     * @throws \Exception
     */
    public function getGenres(): array
    {
        if (empty($this->genres)) {
            throw new \Exception('Please set genres first.');
        }

        return $this->genres;
    }
}