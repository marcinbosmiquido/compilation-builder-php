<?php

namespace AppBundle\Services\Transformer\Resource;

use AppBundle\Services\Database\ArtistRole\ArtistRoleInterface;

/**
 * Interface ArtistRolesResourceInterface
 * @package AppBundle\Services\Transformer\Resource
 */
interface ArtistRolesResourceInterface
{

    /**
     * @return ArtistRoleInterface[]
     * @throws \Exception
     */
    public function getRoles(): array;
}