<?php

namespace AppBundle\Services\Transformer\Resource;

use AppBundle\Services\Database\ArtistRole\ArtistRoleInterface;

class ArtistRolesResource implements ArtistRolesResourceInterface
{

    /**
     * @var ArtistRoleInterface[]
     */
    private $roles = [];


    /**
     * GenresResource constructor.
     *
     * @param array $roles
     */
    public function __construct(array $roles)
    {
        foreach ($roles as $role) {
            $this->addRole($role);
        }
    }


    /**
     * @param ArtistRoleInterface $role
     */
    private function addRole(ArtistRoleInterface $role): void
    {
        $this->roles[] = $role;
    }


    /**
     * @return ArtistRoleInterface[]
     * @throws \Exception
     */
    public function getRoles(): array
    {
        if (empty($this->roles)) {
            throw new \Exception('Please set artist roles first.');
        }

        return $this->roles;
    }

}