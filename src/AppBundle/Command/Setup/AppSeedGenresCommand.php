<?php

namespace AppBundle\Command\Setup;

use AppBundle\Repository\GenreRepository;
use AppBundle\Services\Database\Genre\GenreRepositoryInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class AppSeedGenresCommand
 * @package AppBundle\Command\Setup
 */
class AppSeedGenresCommand extends ContainerAwareCommand
{

    /**
     * @var GenreRepositoryInterface
     */
    private $genreRepository;


    /**
     * AppSeedGenresCommand constructor.
     *
     * @param GenreRepositoryInterface $genreRepository
     */
    public function __construct(GenreRepositoryInterface $genreRepository)
    {
        parent::__construct();
        $this->genreRepository = $genreRepository;
    }


    /**
     *
     */
    protected function configure()
    {
        $this->setName('app:seed-genres');
    }


    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->genreRepository->checkIfNotEmpty();

        try {
            $this->genreRepository->addGenres($this->getGenresData());
        } catch (\Exception $exception) {
            $output->writeln(sprintf('<bg=red;options=bold> %s </>', $exception->getMessage()));
        }

        $output->writeln(sprintf('<bg=green;options=bold>Inserted %s genres</>', count($this->getGenresData())));
    }


    /**
     * @return array
     */
    private function getGenresData(): array
    {
        return [
            "Adult Contemporary",
            "Alternative",
            "Axé",
            "Bhangra",
            "Bluegrass",
            "Blues",
            "Bossa nova",
            "Caribbean",
            "Children",
            "Choro",
            "Christian",
            "Classical",
            "Classical Crossover",
            "Comedy",
            "Country",
            "Dance",
            "Devotional",
            "Disco",
            "Easy Listening",
            "Electronic",
            "Enka",
            "Fado",
            "Fitness & Workout",
            "Flamenco",
            "Folk",
            "Forró",
            "French Pop",
            "Frevo",
            "Funk",
            "Ghazal",
            "Gospel",
            "Hip Hop",
            "Holiday",
            "Indian Classical",
            "Inspirational",
            "Instrumental",
            "J-POP",
            "Jazz",
            "K-POP",
            "Karaoke",
            "Kayokyoku",
            "Latin",
            "Latin / Pop",
            "Latin / Regional Mexican",
            "Latin / Rock",
            "Latin / Tropical",
            "Latin / Urban",
            "MPB",
            "Metal",
            "Motown",
            "Musical",
            "Musique Francophone",
            "Native American",
            "New Age",
            "Pagode",
            "Pop",
            "Punk",
            "Qawwali",
            "R & B",
            "Rap",
            "Reggae",
            "Reggaeton",
            "Rock",
            "Samba",
            "Schlager",
            "Sertanejo",
            "Singer-Songwriter",
            "Soul",
            "Soundtrack",
            "Spoken Word",
            "Swing",
            "Tango",
            "Urban",
            "Vocal",
            "Volksmusik",
            "World",
        ];
    }

}
