<?php

namespace AppBundle\Command\Setup;

use AppBundle\Services\Database\ArtistRole\ArtistRoleRepositoryInterface;
use AppBundle\Services\Database\Genre\GenreRepositoryInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class AppSeedArtistRolesCommand
 * @package AppBundle\Command\Setup
 */
class AppSeedArtistRolesCommand extends ContainerAwareCommand
{

    /**
     * @var ArtistRoleRepositoryInterface
     */
    private $artistRoleRepository;


    /**
     * AppSeedGenresCommand constructor.
     *
     * @param ArtistRoleRepositoryInterface $artistRoleRepository
     */
    public function __construct(ArtistRoleRepositoryInterface $artistRoleRepository)
    {
        parent::__construct();
        $this->artistRoleRepository = $artistRoleRepository;
    }


    /**
     *
     */
    protected function configure()
    {
        $this->setName('app:seed-artistRoles');
    }


    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->artistRoleRepository->checkIfNotEmpty();

        try {
            $this->artistRoleRepository->addRoles($this->getArtistRolesData());
        } catch (\Exception $exception) {
            $output->writeln(sprintf('<bg=red;options=bold> %s </>', $exception->getMessage()));
        }

        $output->writeln(sprintf('<bg=green;options=bold>Inserted %s artist roles</>', count($this->getArtistRolesData())));
    }


    /**
     * @return array
     */
    private function getArtistRolesData(): array
    {
        return [
            "Artist",
            "Composer/Author",
            "Producer",
            "Composer",
            "Author",
            "Conductor",
            "Mixer",
            "Recording Engineer",
            "Orchestra",
        ];
    }

}
