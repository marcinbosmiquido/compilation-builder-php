<?php

namespace AppBundle\Controller;

use AppBundle\Services\Database\ArtistRole\ArtistRoleRepositoryInterface;
use AppBundle\Services\Transformer\Api\ArtistRolesTransformerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class GetGenresController
 * @package AppBundle\Controller
 */
class GetArtistRolesController
{

    /**
     * @var ArtistRoleRepositoryInterface
     */
    private $artistRoleRepository;

    /**
     * @var ArtistRolesTransformerInterface
     */
    private $artistRolesTransformer;


    /**
     * GetArtistRolesController constructor.
     *
     * @param ArtistRoleRepositoryInterface   $artistRoleRepository
     * @param ArtistRolesTransformerInterface $artistRolesTransformer
     */
    public function __construct(ArtistRoleRepositoryInterface $artistRoleRepository, ArtistRolesTransformerInterface $artistRolesTransformer)
    {
        $this->artistRoleRepository = $artistRoleRepository;
        $this->artistRolesTransformer = $artistRolesTransformer;
    }


    /**
     * @Route("resources/artist-roles", name="artist_roles_list")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        return new JsonResponse($this->artistRolesTransformer->transform($this->artistRoleRepository->getArtistRoles()), 200);
    }
}
