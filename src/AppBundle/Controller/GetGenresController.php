<?php

namespace AppBundle\Controller;

use AppBundle\Services\Database\Genre\GenreRepositoryInterface;
use AppBundle\Services\Transformer\Api\GenresTransformerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class GetGenresController
 * @package AppBundle\Controller
 */
class GetGenresController
{

    /**
     * @var GenreRepositoryInterface
     */
    private $genreRepository;

    /**
     * @var GenresTransformerInterface
     */
    private $genresTransformer;


    /**
     * GetGenresController constructor.
     *
     * @param GenreRepositoryInterface   $genreRepository
     * @param GenresTransformerInterface $genresTransformer
     */
    public function __construct(GenreRepositoryInterface $genreRepository, GenresTransformerInterface $genresTransformer)
    {
        $this->genreRepository = $genreRepository;
        $this->genresTransformer = $genresTransformer;
    }


    /**
     * @Route("resources/genres", name="genres_list")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        return new JsonResponse($this->genresTransformer->transform($this->genreRepository->getGenres()), 200);
    }
}
