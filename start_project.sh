#!/usr/bin/env bash

# Connect to Docker Daemon
eval $(docker-machine env default)

# Build Composition Images
docker-compose build

# Run composition
docker-compose up -d