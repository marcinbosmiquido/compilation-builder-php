FROM php:7-apache
RUN apt-get update && apt-get install -y \
        libpng12-dev libcurl4-openssl-dev pkg-config libssl-dev openssl libjpeg-dev supervisor cron
RUN docker-php-ext-configure gd --with-config-file-path=/etc --with-config-file-scan-dir=/etc/php.d --with-apxs2 --with-mysql --with-mysqli --with-zlib --with-curl --with-libdir=lib --with-openssl --with-pdo-mysql --with-mcrypt  --with-pcre-regex --enable-zip --with-gd --enable-mbstring --with-jpeg-dir=/usr/lib
RUN docker-php-ext-install pdo_mysql
RUN pecl channel-update pecl.php.net
RUN pecl install oauth
RUN rm -f /etc/apache2/sites-available/000-default.conf
COPY ./.server/files/000-default.conf /etc/apache2/sites-available/000-default.conf
COPY ./.server/files/php-ini-overrides.ini /usr/local/etc/php/conf.d
COPY . /var/www/html
RUN chown -R www-data:www-data /var/www/html
RUN a2enmod rewrite
RUN export | grep -i 'api\|doctrine\|elastic' | awk '{print "export " $3}' > /etc/environment
EXPOSE 80
